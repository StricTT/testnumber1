<?php

namespace Patterns\Creational\Prototype;


class Book
{
    private $title;

    private $category;

    private $foodBook;

    private $comments = [];


    public function __construct(string $title, string $category, FoodBook $foodBook)
    {
        $this->title = $title;
        $this->category = $category;
        $this->foodBook = $foodBook;
        $this->foodBook->addToPage($this);

    }

    public function addComment(string $comment): void
    {
        $this->comments[] = $comment;
    }

    public function __clone()
    {
        $this->title = "Copy of " . $this->title;
        $this->foodBook->addToPage($this);
        $this->comments = [];

    }
}

class FoodBook
{
    private $name;

    private $books = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addToPage(Book $book): void
    {
        $this->books[] = $book;
    }
}

function clientCode()
{
    $foodBook = new FoodBook("Sam Clark");
    $book = new Book("The Handmade Loaf", "Food", $foodBook);

    $book->addComment("TY for this nice book");

    $draft = clone $book;
    echo "Thats clone. \n\n";
    print_r($draft);
}

clientCode();